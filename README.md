<h2 align="center"> 
    Hackathon Meta
</h2>

## Pessoas que trabalharam no projeto:
<ul>
    <li>[Matheus Castro](https://www.linkedin.com/in/matheuscastro77/) - Frontend</li> 
    <li>[Jardel Santos](https://www.linkedin.com/in/jardel-santos-66a821208/) - Frontend</li>
    <li>[Ana karine](https://www.linkedin.com/in/ana-karine-739b94142/) - Frontend</li>
    <li>[Carlise Garbin](https://www.linkedin.com/in/carlise-garbin-debona/) - Backend</li>
    <li>[Sergio Henrique](https://www.linkedin.com/in/sergio-henrique-7a7ab91aa/) - Backend</li>
</ul>

## Link 
[Link](https://meta-feedback.vercel.app/)

---
## Website
### Login
![login](/uploads/cc8bc3297bd0331890f8a0b88afe8544/login.png)
### Tela incial
![tela_inicial](/uploads/6efa744f02a611c40edcd7205afc04b5/tela_inicial.png)
### Cadastro Mentor/Gestor
![cadastro_mentor](/uploads/6f3d50f0d042f7d78c950d9e15df33b1/cadastro_mentor.png)
### Envio de formulário
![envio_de_formulário](/uploads/d92cc99527ea5f61aa74b5e4c4ea54a8/envio_de_formulário.png)
### Página de cadastro do leaguer
![página_de_cadastro](/uploads/d1a975783455b850efa71fe70c0bfdb4/página_de_cadastro.png)

## Lista de algumas das linguagens ultilizadas
<ul>
    <li>HTML, CSS e JAVASCRIPT</li>
    <li>React Js</li>
    <li>Route System</li>
    <li>Styled-Components, Material-Ui</li>
    <li>Node.Js, Knex, Express, Jwt</li>
    
</ul>
 
## IDE

Visual Studio Code

## Como usar o projeto com NPM

## 1 - Clone o repositório
    - git clone https://gitlab.com/grupo-4-hackathon-grupo-meta/grupo-4-desafio-meta-x-labenu.git
## 2 - Instalar as dependências e iniciar o projeto 

## 2.1 - Comando para instalar as dependências
       - npm install

## 3 - Iniciar o projeto
    - npm start
